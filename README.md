# BI-PYT Image Operations

A simple python image editor Tkinter application.

How to use: 
1) Execute the script
2) Enter the path to your image
3) Edit your image using the buttons
4) Enter the filename and press Save to save the result (saved images are always .png)