#! python3

# author: Petr Bures

from tkinter import *
import numpy as np
import os
from PIL import ImageTk, Image

class App:
	 
	def __init__(self, master, pic):

		# Initialize all the frames
		master_frame = Frame(master, borderwidth=1, relief = 'raised')
		master_frame.pack()

		btn_frame = Frame(master_frame, borderwidth=1)
		btn_frame.pack(expand=1)
		
		img_frame = LabelFrame(master_frame, borderwidth=1, relief = 'sunken')
		img_frame.pack(fill=BOTH, expand=1)

		photo = ImageTk.PhotoImage(pic)
		self.img = Label(img_frame, image=photo, borderwidth=1)
		self.img.photo = photo
		self.img.pack(expand=1)
		
		# Initialize all the buttons and bind them to functions

		self.button_write = Button(btn_frame, text="Save", command=self.write)
		self.button_write.pack(side=RIGHT, padx=5, pady=5)

		self.entry = Entry(btn_frame)
		self.entry.pack(side=RIGHT, padx=5, pady=5)

		self.button_turn = Button(btn_frame, text="Turn", command=self.turn)
		self.button_turn.pack(side=RIGHT, padx=5, pady=5)

		self.button_mirror = Button(btn_frame, text="Mirror", command=self.mirror)
		self.button_mirror.pack(side=RIGHT, padx=5, pady=5)

		self.button_invert = Button(btn_frame, text="Invert", command=self.invert)
		self.button_invert.pack(side=RIGHT, padx=5, pady=5)

		self.button_convert = Button(btn_frame, text="Grayscale", command=self.convert)
		self.button_convert.pack(side=RIGHT, padx=5, pady=5)

		self.button_brighten = Button(btn_frame, text="Brighten", command=self.brighten)
		self.button_brighten.pack(side=RIGHT, padx=5, pady=5)

		self.button_darken = Button(btn_frame, text="Darken", command=self.darken)
		self.button_darken.pack(side=RIGHT, padx=5, pady=5)

		self.button_print = Button(btn_frame, text="Sharpen", command=self.sharpen)
		self.button_print.pack(side=RIGHT, padx=5, pady=5)

		self.turned = 0
		parr = np.array(pic)
		self.res = Image.fromarray(np.uint8(parr))
		self.res.save('res.png')

	def write(self):
		self.res.save(self.entry.get() + '.png')
		print('Saved image to file ' + self.entry.get() + '.png.')

	def update(self):
		self.res.save('res.png')
		photo = ImageTk.PhotoImage(Image.open('res.png'))
		self.img.configure(image=photo)
		self.img.photo = photo
		self.img.pack()
	 
	def turn(self, _event = None):
		parr = np.array(Image.open('res.png'))
		parr = np.transpose(parr, axes=(1, 0, 2))
		# Second transpose returns image into last state, but 180 degree turn can be done by reversing the order
		if(self.turned % 2 == 0):
			self.res = Image.fromarray(np.uint8(parr[::-1,::-1]))
		else:
			self.res = Image.fromarray(np.uint8(parr))
		self.turned = self.turned + 1
		self.update()

	def mirror(self, _event = None):
		parr = np.array(Image.open('res.png'))
		# Add reversed image right of the image
		self.res = Image.fromarray(np.concatenate((np.uint8(parr), np.uint8(parr[::,::-1])), axis = 1))
		self.update()

	def invert(self, _event = None):
		parr = np.array(Image.open('res.png'))
		# Invert all RGB values without changing the alpha channel if present 
		if(parr.shape[2] == 3):
			self.res = Image.fromarray(np.uint8(parr ^ [255, 255, 255]))
		elif(parr.shape[2] == 4):
			self.res = Image.fromarray(np.uint8(parr ^ [255, 255, 255, 0]))
		self.update()

	def convert(self, _event = None):
		parr = np.array(Image.open('res.png'))
		# Count luma values and set them to all RGB values in the array while keeping the alpha channel if present
		if(parr.shape[2] == 4):
			parr = parr * [0.299, 0.587, 0.114, 1]
			sums = np.sum(parr[:,:,:-1], axis = 2)
			self.res = Image.fromarray(np.dstack((np.uint8(sums), np.uint8(sums), np.uint8(sums), np.uint8(parr[:,:,-1:]))))
		elif(parr.shape[2] == 3):
			parr = parr * [0.299, 0.587, 0.114]
			sums = np.sum(parr, axis = 2)
			self.res = Image.fromarray(np.dstack((np.uint8(sums), np.uint8(sums), np.uint8(sums))))
		self.update()

	def brightness(self, val):
		parr = np.array(Image.open('res.png'))
		# Multiply all RGB values and make sure they are valid
		if(parr.shape[2] == 4):
			self.res = Image.fromarray(np.uint8(np.clip(parr * [val, val, val, 1], 0, 255)))
		elif(parr.shape[2] == 3):
			self.res = Image.fromarray(np.uint8(np.clip(parr * [val, val, val], 0, 255)))
		self.update()

	def brighten(self, _event = None):
		self.brightness(1.2)

	def darken(self, _event = None):
		self.brightness(0.8)

	def sharpen(self, _event = None):
		parr = np.array(Image.open('res.png'), dtype=int)
		neighbors = np.zeros(parr[:,:,0:3].shape, dtype=int)
		# Create an array of all neighbors values sums (from Life game optimalizations)
		neighbors[1:-1,1:-1] += (parr[ :-2, :-2, 0:3] + parr[ :-2,1:-1, 0:3] + parr[ :-2,2:, 0:3] +
								 parr[1:-1, :-2, 0:3]						 + parr[1:-1,2:, 0:3] +
								 parr[2:  , :-2, 0:3] + parr[2:  ,1:-1, 0:3] + parr[2:  ,2:, 0:3])
		# Multiply the RGB values of each pixel and subtract their neighbors values 
		# while keeping alpha channel if present and all RGB values valid
		if(parr.shape[2] == 4):
			parr = np.dstack((np.clip(parr[:,:,0:3] * 9 - neighbors, 0, 255), parr[:,:,-1:]))
		elif(parr.shape[2] == 3):
			parr = np.clip(parr[:,:,0:3] * 9 - neighbors, 0, 255)
		self.res = Image.fromarray(np.uint8(parr))
		self.update()


def main():

	# Show all images in the working directory and ask the user to choose
	print('Images in the working directory: ')
	for file in os.listdir():
		if (file.endswith(".jpg") or file.endswith(".png")):
			print(file, end =' ')
	print()
	imgname = input ('Enter the name of the image you want to edit: ')

	root = Tk()
	root.title('Image Editor')

	pic = Image.open(imgname)

	app = App(root, pic)
	root.mainloop()

if __name__ == '__main__':
	main()